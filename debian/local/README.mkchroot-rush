Usage examples for mkchroot-rush.pl
===================================

  In the process of preparing the initial Debian package
  for GNU Rush, a helper Perl program was developed for
  easy construction of minimal chrooted environments.
  The target is of course the present service.

  This text illuminates how the default configuration
  file for 'rush_1.7', i.e., '/etc/rush.rc' can be
  prepared in collaboration with 'mkchroot-rush.pl'.

  For simplicity, perform this unpacking:

    # gunzip -c /usr/share/doc/rush/scripts/mkchroot-rush.pl \
            > /usr/sbin/mkchroot-rush.pl
    # chmod u+x /usr/sbin/mkchroot-rush.pl

    # cp /usr/share/doc/rush/scripts/mkchroot-rush.conf \
            /etc/mkchroot-rush.conf

  The configuration file '/etc/rush.rc' is prepared for a single
  example user Dumle:

    # echo /usr/sbin/rush >> /etc/shells

    # adduser --shell /usr/sbin/rush dumle

  In the following text, the default setting

    $chrootdir == /srv/rush/

  is best remembered.

  In all command line references here, the super user root is
  working on 'remotebox', whereas the user acting as Dumle is
  issuing commands at 'localbox'.

SCP and SFTP
============

  Fairly simple preparations are needed for both services:

    # mkchroot-rush.pl --conf /etc/mkchroot-rush.conf \
           --binaries /usr/bin/scp,/usr/lib/openssh/sftp-server

    #### For GNU/kFreeBSD only
    #
    # mount_devfs devfs /srv/rush/dev/
    #
    ####

    # mkdir  -p  /srv/rush/srv/incoming/{alpha,ftp}
    # chmod  a+w  /srv/rush/srv/incoming/{alpha,ftp}


  At this stage the user dumle is able to do uploads:

    $ scp /etc/issue dumle@remotebox:/incoming/alpha/

    $ scp /etc/issue dumle@remotebox:/incoming/ftp/

  No access is granted above or beside either of '/incoming/alpha/'
  or '/incoming/ftp/'. This is from the perspective of 'dumle'.
  The user is allowed to copy single files to either target directory.

  Server-side on 'remotebox', the targets are mangled to

     /srv/rush/srv/incoming/alpha/

     /srv/rush/srv/incoming/ftp/


  The activation of SFTP is now as simple as hiding the default rule
  'scp-to' found in '/etc/rush.rc' and instead uncommenting the next
  immediately following rule. The key point is that 'mkchroot-rush.pl'
  installs in a simplified path:

     /usr/bin/sftp-server

  The compressed action

     # sed -i -e '81,87s/^/#/' -e '93,99s/^##//' /etc/rush.rc

  Once this change of configuration is made, Dumle may execute

     $ sftp dumle@remotebox

  First he arrives at '/srv/rush/home/dumle', but stays confined
  to the directory '/srv/rush/'. Of course the string '/srv/rush'
  remains invisible for Dumle.

Preparing for RSYNC
===================

  As a continuation of the previous setup, the call

    # mkchroot-rush.pl --conf /etc/mkchroot-rush.conf \
           --binaries /usr/bin/rsync
           --tasks bin,lib --force

  is sufficient. The masking '--force' just accepts the case
  that '/srv/rush/' could be an already existing chroot.
  For a fresh start, the correct call would be the simpler

    # mkchroot-rush.pl --conf /etc/mkchroot-rush.conf \
           --binaries /usr/bin/rsync

  since the default takes care of the rest.

  Either way, Dumle is now able to execute

    $ rsync -a  /etc/default dumle@remotebox:~/

  The initial path '~/' is mandatory, since Dumle is confined
  to act inside '/srv/rush/home/dumle/', i.e., '$chrootdir/~dumle/'.


Going for Git
=============

  The point of departure is now (a long list of binaries)

    # mkchroot-rush.pl --conf /etc/mkchroot-rush.conf \
           --binaries /bin/sh,/bin/sed,/usr/bin/git,/usr/lib/git-core/git-upload-pack,/usr/lib/git-core/git-receive-pack \
           --tasks bin,lib --force

    # mkdir -p /srv/rush/srv/gitroot

  Here Dash and Sed enter the picture since one would want to have
  the option of executing the update hook at any access to the
  repository.

  The administrator could also consider the addition

    # mkchroot-rush.pl --conf /etc/mkchroot-rush.conf \
           --binaries /usr/lib/git-core/git-update-server-info \
           --tasks bin,lib --force

  should a web server ever put the repository on display.

  Given that a bare Git repository has been instantiated in

    /srv/rush/srv/gitroot/snigel.git

  the above calls to 'mkchroot-rush.pl' will enable Dumle
  to get to work with

    $ git clone git+ssh://dumle@remotebox/gitroot/snigel.git

  Observe that the rule 'git-rush' in '/etc/rush.rc' is arranged
  so that the path is prepended with '/srv'. The user is not
  concerned with this mangling.

  Granted that the repository is arranged so that 'dumle' has
  write access, he will also be able to contribute back:

    $ git push

  The update hook will prevent incomplete actions, which is
  why '/bin/sh' and '/bin/sed' are worth the effort.


Using Subversion
================

  Simpler this time around:

    # mkchroot-rush.pl --conf /etc/mkchroot-rush.pl \
          --binaries /usr/bin/svnserve \
          --tasks bin,lib --force

    # mkdir -p /srv/rush/srv/svnroot

    # svnadmin create /srv/rush/srv/svnroot/our-book

  Being lazy for once,

    # chown -R dumle:dumle /srv/rush/srv/svnroot/our-book

  This paves the way for a future author:

    $ svn mkdir svn+ssh://dumle@remotebox/our-book/chapter1

  and the story commences. Observe here, that the default
  setup in '/etc/rush.rc' is arranged to prepend '/srv/svnroot'
  to any path the user cares to specify.


CVS is there
============

  Add the binary and its dependencies:

    # mkchroot-rush.pl --conf /etc/mkchroot-rush.pl \
          --binaries /usr/bin/cvs \
          --tasks bin,lib --force

    # mkdir -p /srv/rush/srv/cvsroot
    # mkdir /srv/rush/tmp
    # chmod a+w /srv/rush/tmp

    # cvs -d /srv/rush/srv/cvsroot init

  Remember that Dumle will need write capability in
  '/srv/rush/srv/cvsroot/CVSROOT/' and also in each
  relevant module, otherwise he is not even allowed
  a common checkout.

  Next import something into the repository; let us call
  the new module 'our-book'. Then Dumle can start working:

    $ cvs -d :ext:dumle@remotebox:/srv/cvsroot co our-book

  and he can add files and commit as usual.

